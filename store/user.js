export const state = () => ({
  users: []
})

export const mutations = {}

export const actions = {
  async createUser ({ commit }, payload) {
    try {
      await console.log('User created', payload)
    } catch (e) {
      console.log(e.message)
    }
  }
}

export const getters = {}
