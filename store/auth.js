export const state = () => ({
  token: true
})

export const mutations = {
  setStateToken (state, payload) {
    state.token = payload
  },
  clearToken (state) {
    state.token = null
  }
}

export const actions = {
  async login ({ dispatch, commit }, payload) {
    try {
      const { token } = await this.$axios.$post('/api/auth/admin/login', payload)
      dispatch('setToken', token)
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
    // return await this.$axios.$post('/api/auth/admin/login', payload)
    //   .then(res => console.log(res))
    //   .catch(e => console.log('e', e))
  },
  setToken ({ commit }, payload) {
    commit('setStateToken', payload)
  },
  logout ({ commit }) {
    commit('clearToken')
  }
}

export const getters = {
  isAuth: state => !!state.token
}
