const posts = [
  {
    title: 'Post 1', date: new Date(), views: 22, comments: [1, 2], _id: 123
  },
  {
    title: 'Post 2', date: new Date(), views: 221, comments: [1, 2, 3], _id: 2342
  }
]

export const actions = {
  async getAdminPosts () {
    return await new Promise((resolve) => {
      setTimeout(() => {
        resolve(posts)
      }, 2000)
    })
  },
  async deleteAdminPost () {
    await console.log('post was deleted')
  },
  async getPostById ({ commit }, id) {
    return await new Promise((resolve) => {
      setTimeout(() => {
        resolve(posts.find(item => +item._id === +id))
      }, 2000)
    })
  },
  async editAdminPost ({ commit }, post) {
    await console.log('post was edited', post)
  },
  async createAdminPost ({ commit }, post) {
    console.log(post)
    try {
      const fd = new FormData()
      fd.append('title', post.title)
      fd.append('text', post.text)
      fd.append('image', post.image, post.image.name)

      return await new Promise((resolve) => {
        setTimeout(() => {
          resolve(fd)
        }, 2000)
      })
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  }
}
