import Vue from 'vue'
import VueMarkdown from 'vue-markdown/src/VueMarkdown'

Vue.component('vue-markdown', VueMarkdown)
