const bcrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')
const User = require('../models/user.model')

module.exports.login = async (req, res) => {
  const candidate = await User.findOne({ login: req.body.login })
  if (candidate) {
    const isCorrect = bcrypt.compareSync(req.body.password, candidate.password)
    console.log(isCorrect)
    if (isCorrect) {
      const token = jwt.sign({
        login: candidate.login,
        id: candidate._id
      }, '', 60 * 60)

      res.json({ token })
    } else {
      res.status(401).json({ message: 'Wrong password' })
    }
  } else {
    res.status(404).json({ message: 'User not found' })
  }
}

module.exports.createUser = (req, res) => {
}
