const { loadNuxt, build } = require('nuxt')
// const config = require('../nuxt.config')

const isDev = process.env.NODE_ENV !== 'production'
const app = require('./app')
const port = process.env.PORT || 3000

async function start () {
  // We get Nuxt instance
  const nuxt = await loadNuxt(isDev ? 'dev' : 'start')

  // Render every route with Nuxt.js
  app.use(nuxt.render)

  // Build only in dev mode with hot-reloading
  if (isDev) {
    build(nuxt)
  }
  // Listen the server
  app.listen(port, () => {
    console.log(`API server listening on port ${port}`)
  })
}

start()

module.exports = {
  path: '/api',
  handler: app
}
