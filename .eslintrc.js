module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  plugins: [
  ],
  rules: {
    'no-console': 'off',
    'vue/attribute-hyphenation': 0,
    'vue/require-prop-types': 0,
    'vue/valid-v-slot': ['error', {
      allowModifiers: true
    }]
  }
}
